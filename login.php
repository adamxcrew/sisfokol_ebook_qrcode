<?php
//////////////////////////////////////////////////////////////////////
// SISFOKOL-EBOOK-QRCODE v1.0                                       //
// SISFOKOL khusus sistem perpustakaan ebook pdf sederhana,         //
// dengan metode login peminjam dengan qrcode scan.                 //
//////////////////////////////////////////////////////////////////////
// Dikembangkan oleh : Agus Muhajir                                 //
// E-Mail : hajirodeon@gmail.com                                    //
// HP/SMS/WA : 081-829-88-54                                        //
// source code :                                                    //
//   http://github.com/hajirodeon                                   //
//   http://gitlab.com/hajirodeon                                   //
//////////////////////////////////////////////////////////////////////




//ambil nilai
require("inc/config.php");
require("inc/fungsi.php");
require("inc/koneksi.php");
require("inc/class/paging.php");
$tpl = LoadTpl("template/login.html");


nocache;

//nilai
$filenya = "login.php";
$judul = "LOGIN USER - EBOOK SYSTEM";
$pesan = "QRCODE TIDAK DITEMUKAN atau SALAH. Silahkan Coba Lagi...!!!";
$artkd = nosql($_REQUEST['artkd']);



//isi *START
ob_start();


//null
if (empty($artkd))
	{
	?>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	
	<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
	
	
	
	<video id="preview"></video>
	
	<script type="text/javascript">
	
	  let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
	
	  scanner.addListener('scan', function (content) {
	
	    //alert(content);
	    window.location.href = "login.php?artkd="+content; 
	
	  });
	
	  Instascan.Camera.getCameras().then(function (cameras) {
	
	    if (cameras.length > 0) {
	
	      scanner.start(cameras[0]);
	
	    } else {
	
	      console.error('No cameras found.');
	
	    }
	
	  }).catch(function (e) {
	
	    console.error(e);
	
	  });
	
	</script>
	
	   
	
	
	<?php
	}
	
else
	{
	//query
	$q = mysqli_query($koneksi, "SELECT * FROM m_orang ".
				"WHERE kd = '$artkd'");
	$row = mysqli_fetch_assoc($q);
	$total = mysqli_num_rows($q);
	
	//cek login
	if ($total != 0)
		{
		session_start();
	
		//nilai
		$_SESSION['kd1_session'] = nosql($row['kd']);
		$_SESSION['no1_session'] = balikin($row['kode']);
		$_SESSION['nm1_session'] = balikin($row['nama']);
		$_SESSION['tipe_session'] = "ANGGOTA";
		$_SESSION['hajirobe_session'] = $hajirobe;
	
	
		//re-direct
		$ke = "user.php";
		xloc($ke);
		exit();
		}
	else
		{
		//re-direct
		pekem($pesan,$filenya);
		exit();
		}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
	}
	
	
	
	
	
//isi
$isi = ob_get_contents();
ob_end_clean();

require("inc/niltpl.php");


//diskonek
xclose($koneksi);
exit();
?>
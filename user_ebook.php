<?php
session_start();

//////////////////////////////////////////////////////////////////////
// SISFOKOL-EBOOK-QRCODE v1.0                                       //
// SISFOKOL khusus sistem perpustakaan ebook pdf sederhana,         //
// dengan metode login peminjam dengan qrcode scan.                 //
//////////////////////////////////////////////////////////////////////
// Dikembangkan oleh : Agus Muhajir                                 //
// E-Mail : hajirodeon@gmail.com                                    //
// HP/SMS/WA : 081-829-88-54                                        //
// source code :                                                    //
//   http://github.com/hajirodeon                                   //
//   http://gitlab.com/hajirodeon                                   //
//////////////////////////////////////////////////////////////////////



//ambil nilai
require("inc/config.php");
require("inc/fungsi.php");
require("inc/koneksi.php");
require("inc/class/paging.php");
$tpl = LoadTpl("template/user.html");


nocache;



//nilai
$kd = balikin($_SESSION['kd1_session']);
$kode = balikin($_SESSION['no1_session']);
$nama = balikin($_SESSION['nm1_session']);
$filenya = "user_ebook.php";
$judul = "KOLEKSI BUKU";
$judulku = "[$kode.$nama]. $judul";
$s = nosql($_REQUEST['s']);
$artkd = nosql($_REQUEST['artkd']);


$limit = 10000;



//isi *START
ob_start();

?>




			
<script>
$(document).ready(function() {
  		
	$.noConflict();
    
});
</script>
  



<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css">


    

    
			
			<style>
			table{
			    width:100%;
			}
			
			
			td.highlight {
			    background-color: whitesmoke !important;
			}
			</style>
			
			
			<script>
			
			$(document).ready(function() {
			
			$.noConflict();
					 
			    $('#example').DataTable( {
			        "scrollY": "100%",
			        "scrollX": true,
					"lengthChange": false, 
					"pageLength": 10, 
					"language": {
								"url": "Indonesian.json",
								"sEmptyTable": "Tidak ada data di database"
							}
			    } );
			    
			    
			    var table = $('#example').DataTable();
			     
			    $('#example tbody')
			        .on( 'mouseenter', 'td', function () {
			            var colIdx = table.cell(this).index().column;
			 
			            $( table.cells().nodes() ).removeClass( 'highlight' );
			            $( table.column( colIdx ).nodes() ).addClass( 'highlight' );
			        } );
			
			
			
			} );
			
			
			
			</script>
			




<?php
if (empty($s))
	{
	?>
	<div class="row">
	<div class="col-md-12">
	
		<div class="box">
		    <div class="box-header with-border">
		      <h3 class="box-title">KOLEKSI EBOOK</h3>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
		      <div class="row">
		        <div class="col-md-12">
	
	
				<?php
					//query
					$p = new Pager();
					$start = $p->findStart($limit);
					
					$sqlcount = "SELECT * FROM m_item ".
									"ORDER BY nama ASC";
					$sqlresult = $sqlcount;
					
					$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
					$pages = $p->findPages($count, $limit);
					$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
					$pagelist = $p->pageList($_GET['page'], $pages, $target);
					$data = mysqli_fetch_array($result);
				
				
				
				
					
					echo '<table id="example" class="table table-striped table-bordered row-border hover order-column" style="width:100%">
				    <thead>
							<tr bgcolor="'.$warnaheader.'">
								<td width="150"><strong><font color="'.$warnatext.'">IMAGE</font></strong></td>
								<td><strong><font color="'.$warnatext.'">NAMA</font></strong></td>
							</tr>
				
					    </thead>
					    <tbody>';
					
						do 
							{
							if ($warna_set ==0)
								{
								$warna = $warna01;
								$warna_set = 1;
								}
							else
								{
								$warna = $warna02;
								$warna_set = 0;
								}
					
							$nomer = $nomer + 1;
							$i_kd = nosql($data['kd']);
							$i_kode = balikin($data['kode']);
							$i_nama = balikin($data['nama']);
							$i_postdate = balikin($data['postdate']);
							$i_ringkasan = balikin($data['ringkasan']);
							$i_qrcode = balikin($data['qrcode']);
							$i_filex1 = balikin($data['filex1']);
							$i_filex_pdf = balikin($data['filex_pdf']);
							$i_jml_dilihat = balikin($data['jml_dilihat']);
							$i_akses = $i_kode;
					
					
							$nil_foto1 = "$sumber/filebox/item/$i_kd/$i_filex1";
							$nil_pdf = "$sumber/filebox/pdf/$i_kd/$i_kd-1.pdf";
				
				
	
					
							echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
							echo '<td>
							
							<img src="'.$nil_foto1.'" width="150">
							
							</td>
							<td>
							<b>'.$i_nama.'</b>
							<br>
							[Postdate : <b>'.$i_postdate.'</b>]. 
							[Dilihat : <b>'.$i_jml_dilihat.'</b>]. 
							<br>
							<br>
							<i>'.$i_ringkasan.'</i>
							<br>
							<br>
							<br>
							<a href="'.$filenya.'?s=baca&artkd='.$i_kd.'" class="btn btn-danger">BACA SELENGKAPNYA >></a>
							</td>
					        </tr>';
							}
						while ($data = mysqli_fetch_assoc($result));
					
					echo '</tbody>
					
						<tfoot>
				
							<tr bgcolor="'.$warnaheader.'">
								<td width="150"><strong><font color="'.$warnatext.'">IMAGE</font></strong></td>
								<td><strong><font color="'.$warnatext.'">NAMA</font></strong></td>
							</tr>
					
						</tfoot>
				
					  </table>';
					  ?>
	
	
	
				
	            </div>
	           </div>
	           </div>
	          </div>
	
	
	   </div>
	  </div>
	<?php
	}

else
	{
	//detail
	$qx = mysqli_query($koneksi, "SELECT * FROM m_item ".
						"WHERE kd = '$artkd'");
	$rowx = mysqli_fetch_assoc($qx);
	$e_nip = balikin($rowx['kode']);
	$e_nama = balikin($rowx['nama']);
	$e_postdate = balikin($rowx['postdate']);
	$e_ringkasan = balikin($rowx['ringkasan']);
	$e_jml_dilihat = balikin($rowx['jml_dilihat']);
	$e_filex1 = balikin($rowx['filex1']);
	
	$nil_foto1 = "$sumber/filebox/item/$artkd/$e_filex1";
	$nil_pdf = "$sumber/filebox/pdf/$artkd/$artkd-1.pdf";
	
	
	
	//update jml dilihat
	$e_jmlnya = $e_jml_dilihat + 1;
	
	//update
	mysqli_query($koneksi, "UPDATE m_item SET jml_dilihat = '$e_jmlnya' ".
								"WHERE kd = '$artkd'");
	
	?>
	<div class="row">
	<div class="col-md-12">
	
	
	<a href="<?php echo $filenya;?>" class="btn btn-danger"><< KEMBALI KE DAFTAR KOLEKSI</a>
	
		<div class="box">
		    <div class="box-header with-border" align="center">
		      <h3 class="box-title"><b><?php echo $e_nama;?></b></h3>
		    </div>
		    <!-- /.box-header -->
		    <div class="box-body">
		      <div class="row">
		        <div class="col-md-12" align="center">
	
					<p>
						<img src="<?php echo $nil_foto1;?>" width="250">
					</p>
					<br>
					
					<p>
						[Postdate : <b><?php echo $e_postdate;?></b>]. 
						[Dilihat : <b><?php echo $e_jml_dilihat;?></b>].					
					</p>	
					<br>
					
					<p>
						<i><?php echo $e_ringkasan;?></i>
					</p>
					<br>
					
					<p>
						<embed src="<?php echo $nil_pdf;?>" width="100%" height="500"></embed>
					</p>
	
				
	            </div>
	           </div>
	           </div>
	          </div>
	
	
	   </div>
	  </div>


	<?php
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//isi
$isi = ob_get_contents();
ob_end_clean();

require("inc/niltpl.php");

//diskonek
xfree($qbw);
xclose($koneksi);
exit();
?>